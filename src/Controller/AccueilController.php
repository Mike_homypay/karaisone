<?php

namespace App\Controller;

use App\Service\RecuperationDeDonnees;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AccueilController extends AbstractController
{
    public function __construct(
        private RecuperationDeDonnees $recuperationDeDonnees
    ) {
    }
    #[Route('/', name: 'app_accueil')]
    public function index(): Response
    {
        $images = $this->recuperationDeDonnees->carouselImage();
        $cardImages = $this->recuperationDeDonnees->cardImage();
        return $this->render('accueil/index.html.twig', [
            'images' => $images,
            'cardImages' => $cardImages,
        ]);
    }
}
