<?php

namespace App\Service;

final class RecuperationDeDonnees
{
    public function carouselImage()
    {
        $images = [
            [
                "path" => "/images/25bc56_3ca2b3b1ea6a472992289c200b214445~mv2.webp",
                "alt" => "Travaux de rénovation"
            ],
            [
                "path" => "/images/25bc56_f97ddc8658904089bdf89776d0a16c1b~mv2.webp",
                "alt" => "Travaux de rénovation"
            ],
            [
                "path" => "/images/11062b_bc75ed7edb924e29999434f9326fdf18~mv2.webp",
                "alt" => "Travaux de rénovation"
            ],
            [
                "path" => "/images/11062b_db5cab9df11a4cbe8496f146f21f9310~mv2.webp",
                "alt" => "Travaux de rénovation"
            ]
        ];
        return $images;
    }

    public function cardImage()
    {
        $images = [
            [
                "title" => "Pourquoi nous choisir ?",
                "path" => "/images/cardImages/pexels-rene-asmussen-3990359.jpg",
                "alt" => "Homme qui fait des travaux",
                "texts" => [
                    "Rapport d'intervention détaillée",
                    "Proposition de solution et adaptation aux situations imprévisibles",
                    "Interlocuteur dédié à chaque dossier",
                    "Outil de gestion des interventions",
                    "Devis gratuit"
                ]
            ],
            [
                "title" => "Plan design 3D",
                "path" => "/images/cardImages/pexels-mark-2724748.jpg",
                "alt" => "Jolie cuisine rénovée",
                "texts" => [
                    "Réalisation des plans en 3D pour une visualisation palpable de votre projet.",
                    "Conseil et expertise sur la faisabilité de votre projet.",
                ]
            ],
            [
                "title" => "Service de gestion et suivi des travaux",
                "path" => "/images/cardImages/pexels-nappy-935949.jpg",
                "alt" => "Suivi de",
                "texts" => [
                    "Service de gestion et suivi des travaux",
                    "Il est dédié au suivi, à la bonne tenue et la réalisation de vos travaux.",
                    "Il a pour mission de coordonner votre demande à l'action de nos équipes terrains pour un rendu parfait de vos projets dans le respect des délais de traitement."
                ]
            ],
        ];

        return $images;
    }
}
